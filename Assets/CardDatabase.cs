using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class CardDatabase : MonoBehaviour
{
    int status;
    bool cardChoosed;

    public GameObject[] cardParent;
    public CardAttribute[] cardShowing;
    public Sprite[] cardSprite;
    public string[] cardName;
    public string[] cardDesc;
    public float[] stamina, speed, hp, armor;
    public AudioSource revealedSFX;
    // Start is called before the first frame update
    void Start()
    {
        status = Random.Range(0, 2);
    }

    // Update is called once per frame
    void Update()
    {
        if(status==0)
        {
            cardShowing[0].imageTarget.sprite = cardSprite[0];
            cardShowing[1].imageTarget.sprite = cardSprite[1];
            cardShowing[2].imageTarget.sprite = cardSprite[2];

            cardShowing[0].cardName.text = cardName[0].ToString();
            cardShowing[1].cardName.text = cardName[1].ToString();
            cardShowing[2].cardName.text = cardName[2].ToString();

            cardShowing[0].cardDesc.text = cardDesc[0].ToString();
            cardShowing[1].cardDesc.text = cardDesc[1].ToString();
            cardShowing[2].cardDesc.text = cardDesc[2].ToString();

            cardShowing[0].stamina.text = "Stamina: " + stamina[0].ToString();
            cardShowing[1].stamina.text = "Stamina: " + stamina[1].ToString();
            cardShowing[2].stamina.text = "Stamina: " + stamina[2].ToString();

            cardShowing[0].speed.text = "Speed: " + speed[0].ToString();
            cardShowing[1].speed.text = "Speed: " + speed[1].ToString();
            cardShowing[2].speed.text = "Speed: " + speed[2].ToString();

            cardShowing[0].hp.text = "Hp: " + hp[0].ToString();
            cardShowing[1].hp.text = "Hp: " + hp[1].ToString();
            cardShowing[2].hp.text = "Hp: " + hp[2].ToString();

            cardShowing[0].armor.text = "Armor: " + armor[0].ToString();
            cardShowing[1].armor.text = "Armor: " + armor[1].ToString();
            cardShowing[2].armor.text = "Armor: " + armor[2].ToString();
        }

        if(status==1)
        {
            cardShowing[0].imageTarget.sprite = cardSprite[1];
            cardShowing[1].imageTarget.sprite = cardSprite[2];
            cardShowing[2].imageTarget.sprite = cardSprite[0];

            cardShowing[0].cardName.text = cardName[1].ToString();
            cardShowing[1].cardName.text = cardName[2].ToString();
            cardShowing[2].cardName.text = cardName[0].ToString();

            cardShowing[0].cardDesc.text = cardDesc[1].ToString();
            cardShowing[1].cardDesc.text = cardDesc[2].ToString();
            cardShowing[2].cardDesc.text = cardDesc[0].ToString();

            cardShowing[0].stamina.text = "Stamina: " + stamina[1].ToString();
            cardShowing[1].stamina.text = "Stamina: " + stamina[2].ToString();
            cardShowing[2].stamina.text = "Stamina: " + stamina[0].ToString();

            cardShowing[0].speed.text = "Speed: " + speed[1].ToString();
            cardShowing[1].speed.text = "Speed: " + speed[2].ToString();
            cardShowing[2].speed.text = "Speed: " + speed[0].ToString();

            cardShowing[0].hp.text = "Hp: " + hp[1].ToString();
            cardShowing[1].hp.text = "Hp: " + hp[2].ToString();
            cardShowing[2].hp.text = "Hp: " + hp[0].ToString();

            cardShowing[0].armor.text = "Armor: " + armor[1].ToString();
            cardShowing[1].armor.text = "Armor: " + armor[2].ToString();
            cardShowing[2].armor.text = "Armor: " + armor[0].ToString();
        }

        if (status == 2)
        {
            cardShowing[0].imageTarget.sprite = cardSprite[2];
            cardShowing[1].imageTarget.sprite = cardSprite[1];
            cardShowing[2].imageTarget.sprite = cardSprite[0];

            cardShowing[0].cardName.text = cardName[2].ToString();
            cardShowing[1].cardName.text = cardName[1].ToString();
            cardShowing[2].cardName.text = cardName[0].ToString();

            cardShowing[0].cardDesc.text = cardDesc[2].ToString();
            cardShowing[1].cardDesc.text = cardDesc[1].ToString();
            cardShowing[2].cardDesc.text = cardDesc[0].ToString();

            cardShowing[0].stamina.text = "Stamina: " + stamina[2].ToString();
            cardShowing[1].stamina.text = "Stamina: " + stamina[1].ToString();
            cardShowing[2].stamina.text = "Stamina: " + stamina[0].ToString();

            cardShowing[0].speed.text = "Speed: " + speed[2].ToString();
            cardShowing[1].speed.text = "Speed: " + speed[1].ToString();
            cardShowing[2].speed.text = "Speed: " + speed[0].ToString();

            cardShowing[0].hp.text = "Hp: " + hp[2].ToString();
            cardShowing[1].hp.text = "Hp: " + hp[1].ToString();
            cardShowing[2].hp.text = "Hp: " + hp[0].ToString();

            cardShowing[0].armor.text = "Armor: " + armor[2].ToString();
            cardShowing[1].armor.text = "Armor: " + armor[1].ToString();
            cardShowing[2].armor.text = "Armor: " + armor[0].ToString();
        }

        if(cardChoosed==true)
        {
            cardParent[0].GetComponentInChildren<Button>().enabled = false;
            cardParent[1].GetComponentInChildren<Button>().enabled = false;
            cardParent[2].GetComponentInChildren<Button>().enabled = false;
        }
    }

    public void OpenCard(int cardnum)
    {
        cardChoosed = true;
        revealedSFX.Play();

        if(cardnum==0)
        {
            cardParent[0].GetComponent<Animator>().Play("CardRevealed1");
        }

        if(cardnum==1)
        {
            cardParent[1].GetComponent<Animator>().Play("CardRevealed2");
        }

        if(cardnum==2)
        {
            cardParent[2].GetComponent<Animator>().Play("CardRevealed3");
        }
    }
}
