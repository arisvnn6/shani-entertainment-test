using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class CardAttribute : MonoBehaviour
{
    public Image imageTarget;
    public TextMeshProUGUI cardName, cardDesc;
    public TextMeshProUGUI stamina, speed, hp, armor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
