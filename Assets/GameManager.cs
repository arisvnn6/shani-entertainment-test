using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject startPanel, gamePanel, starterInstruction;
    public AudioSource buttonSFX;

    public void StartGame()
    {
        startPanel.SetActive(false);
        gamePanel.SetActive(true);
        buttonSFX.Play();
    }

    public void TapToStart()
    {
        starterInstruction.SetActive(false);
        buttonSFX.Play();
    }

    public void Back()
    {
        SceneManager.LoadScene("Main");
        buttonSFX.Play();
    }
}
